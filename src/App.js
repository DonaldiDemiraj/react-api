import './App.css';
import React, {useEffect,useState} from "react"; 
import Recipe from './Recipe' 

const App = () =>{

  const APP_ID = 'your id';
  const APP_KEY = "your key";

  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState('');
  const [query, setQuery] = useState('chicken');

  useEffect( () => {
    getRecipes();
  }, [query]);

  const getRecipes = async () => {
    const response = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`);
    const data = await response.json();
    console.log(data.hits);
    setRecipes(data.hits);

    // fetch(https://api.edamam.com)
    // .then(response => {
    //   response.json
    // })
  };  

  const updateSearch = e => {
    setSearch(e.target.value);
  }

  const getSearch = e => {
    e.preventDefault();
    setQuery(search);
    setSearch('');
  }
 

  // return  (JSX attribute) React.HtmlAttributes<T>.className?: string
  return(
    <div className="App">
      <form onSubmit={getSearch} className="search-form">
        <input type="text" className="search-bar" value={search} onChange={updateSearch} />
        <button type="submit" className="search-button">Search</button>
      </form>
      <div className="recipes">
        {recipes.map(recipe =>(
            <Recipe
              key={recipe.recipe.label}
              title={recipe.recipe.label}
              calories={recipe.recipe.calories}
              image={recipe.recipe.image}
              ingerdients={recipe.recipe.ingredients}
            />
        ))};
      </div>
    </div>
  );
};

export default App;
